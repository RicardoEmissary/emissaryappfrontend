/*Documento js para deshabilitar botones e inputs mientras este activo el tour*/
$(document).ready(function() {
    function controlTour(control) {
        console.log(control)
        $('.form-control').prop("disabled", control);
        $('#AgregarSaldo').prop("disabled", control);

        // Tables
        $('#filterBtn').prop("disabled", control);
        $('#searchBar').prop("disabled", control);

        // cancelaciones.html
        $('.addBtnTable').prop("disabled", control);

        $('.btn-secondary').prop("disabled", control);
        $('.btn-primary').prop("disabled", control);
        $('.form-control').prop("disabled", control);
        $('.btn-outline-primary').prop("disabled", control);
        $('.btn-outline-dark').prop("disabled", control)

        //Envios-Remitente-Destinatario.html
        $('#btn-regresar-remitente-destinatario').prop("disabled", control);

        //Preferencias
        $('#paq-fedex').prop("disabled", control);

        //editar-plantilla-email
        $('.inputfile').prop("disabled", control);

        // cancelaciones.html
        $('.addBtnTable').prop("disabled", control);

        //dashboard.html
        $('.dashboardBtns').prop("disabled", control);

        // editar-perfil.html
        $('.editarPerfilBtns').prop("disabled", control);
        $('.nuevoPerfil-button-steps').prop("disabled", control);

        // envio-informacion.html
        $('.envioInfoBtns').prop("disabled", control);

        // orden-origen.html
        $('.ordenOrigenBtns').prop("disabled", control);

        // paqueterias.html
        $('.paqueteriaBtns').prop("disabled", control);

        // plantilla-email.html
        $('.plantillaEmailBtns').prop("disabled", control);

        // setup.html
        $('.setupBtns').prop("disabled", control);

        // integraciones.html
        $('.integracionesBtns').prop("disabled", control);

        // Links
        if (control) {
            $('.tourLinks').addClass('disabledLink');
        } else {
            $('.tourLinks').removeClass('disabledLink');
        }
    }


    $('#initTour').click(function() {
        controlTour(true);

        /*Click para cuando sale del tour*/
        $('.introjs-skipbutton').click(function() {
            controlTour(false);
        });

        $('.introjs-overlay').click(function() {
            controlTour(false);
        });
    });
})