﻿
$(document).ready(function () {





    setTimeout(function () {
        $("#contenedor").fadeOut("slow");
    }, 1000);




    setTimeout(function () {
        $(".wrapper").fadeIn("slow");
    }, 1500);



    var saldo = $("#txtSaldo").val();
    $("#spanSaldo").append(saldo);

    $("#userSaldo").append(saldo);

    var nombre = $("#txtNombre").val();
    $("#spanNombre").empty().append(nombre);

    var tipoUsuario = $("#txtTipoUsuario").val();

    if (tipoUsuario == 1) {

        $(".nav-item").removeClass("userTipo2");

    }

    $("#btnAgregarSaldo").click(function () {


        $('#mdlAgregarSaldo').modal('show');


    });


    $(".cerrarSesion").click(function () {


        cerrarSesion()


    });
    $("input[name='inlineRadioOptions']").click(function () {
        $("#votar").prop("disabled", false);
    })

});


function imprimirdiv(elem, nombre, trackingNo, orden) {
    $("#clienteDetalle").html(nombre);
    var mywindow = window.open('', 'PRINT', 'height=800,width=800');
    
    mywindow.document.write('<html><head><title>Orden #' + orden + '</title>');
    mywindow.document.write('<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />');
    mywindow.document.write('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />');
    mywindow.document.write('<link rel = "stylesheet" href = "../assets/css/material-dashboard.css?v=2.0.1"> ');
    mywindow.document.write('<link href = "../assets/assets-for-demo/demo.css" rel = "stylesheet"/> ');
    mywindow.document.write('<link href = "css/estilos.css" rel = "stylesheet"/> ');

    mywindow.document.write('<link rel = "stylesheet" href = "https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity = "sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin = "anonymous" > ');

    mywindow.document.write('<link href = "css/openpay.css" rel = "stylesheet" /> ');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title + '</h1>');
    mywindow.document.write('<h2 style="text-align:center;color:black">Cliente: ' + nombre + '</h3>');
    if (trackingNo != undefined) {
        mywindow.document.write('<h2 style="text-align:center">Tracking No: ' + trackingNo + '</h3>');
    }
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body><script>window.onload = function() {window.print();window.close();}</script></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
  
    return true;
}
function wait(ms) {
    var d = new Date();
    var d2 = null;
    do { d2 = new Date(); }
    while (d2 - d < ms);
}

function cerrarSesion() {


    $.ajax({
        type: "POST",
        url: "../Login.aspx/CerrarSesion",
        //data: JSON.stringify({ pJData: pJData }),
        async: false,
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {

            alert("Sesión Finalizada")

            window.location.assign('../IniciarSesion.aspx')
        },
        error: function (result) {

            alert("Favor de cerrar y abrir nueva pestaña")
            //alert("No se hizo el calculo, faltaron datos por llenar")


        }
    });
}


var FechaSinFormato = moment(Date.now());
var fechaConFormato = FechaSinFormato.tz('America/Monterrey').format('YYYY-MM-DD HH:mm:ss');


function fechaActual() {

    var FechaSinFormato = moment(Date.now());
    var fechaConFormato = FechaSinFormato.tz('America/Monterrey').format('YYYY-MM-DD hh:mm:ss');


    return fechaConFormato;
}

function contestarEncuesta() {
    let Error = '';
    let datosEnvio = [];
    var pJData = [];
    pJData.push({
        respuesta_id: $("input[name='inlineRadioOptions']:checked").val(),
    });
    pJData = JSON.stringify(pJData);
    $.ajax({
        type: "POST",
        url: "listadoEnvios.aspx/contestarEncuesta",
        data: JSON.stringify({ pJData: pJData }),
        dataType: 'json',
        async: false,
        cache: false,
        contentType: "application/json; charset=utf-8",
        success: function (datos) {
            var existeError = datos.d.indexOf('Error');
            if (existeError == -1) {
                alert("¡Gracias por su respuesta!");
                $("#cerrarMensaje").click();
            } else {
                Error = datos.d;
                if (Error != "") {
                    alert(Error);
                    return false;
                }
            }
        }
    });
    return datosEnvio;
}

var loader = Math.floor((Math.random() * 7) + 1);
$("#loader").removeClass("loader");
$("#loader").addClass("loader" + loader);