function infoCard($CantidadClientesP, $CantidadEnviosP) {
    $('#cantidad-de-clientes').text($CantidadClientesP);
    $('#cantidad-de-envios').text($CantidadEnviosP);
}

function seleccionarEstado($EstadoP) {

    switch ($EstadoP) {

        case 'Aguascalientes':
            infoCard(85, 250);
            break;

        case 'Baja California Norte':
            infoCard(2, 12);
            break;

        case 'Baja California Sur':
            infoCard(31, 25);
            break;

        case 'Campeche':
            infoCard(54, 23);
            break;

        case 'Chihuahua':
            infoCard(98, 123);
            break;

        case 'Chiapas':
            infoCard(98, 265);
            break;

        case 'Coahuila':
            infoCard(58, 32);
            break

        case 'Colima':
            infoCard(102, 250);
            break;

        case 'Distrito Federal':
            infoCard(45, 98);
            break;

        case 'Durango':
            infoCard(23, 874);
            break;

        case 'Guerrero':
            infoCard(54, 632);
            break;

        case 'Guanajuato':
            infoCard(62, 57);
            break;

        case 'Hidalgo':
            infoCard(78, 389);
            break;

        case 'Jalisco':
            infoCard(25, 545);
            break;

        case 'Edo. Mexico':
            infoCard(65, 124);
            break;

        case 'Michoacán':
            infoCard(16, 874);
            break;

        case 'Morelos':
            infoCard(45, 84);
            break;

        case 'Nayarit':
            infoCard(36, 39);
            break;

        case 'Nuevo León':
            infoCard(14, 39);
            break;

        case 'Oaxaca':
            infoCard(13, 40);
            break;

        case 'Puebla':
            infoCard(23, 97);
            break;

        case 'Queretaro':
            infoCard(12, 17);
            break;

        case 'Quintana Roo':
            infoCard(25, 98);
            break;

        case 'Sinaloa':
            infoCard(65, 87);
            break;

        case 'San Luis Potosí':
            infoCard(21, 36);
            break;

        case 'Sonora':
            infoCard(13, 78);
            break;

        case 'Tabasco':
            infoCard(31, 91);
            break;

        case 'Tamaulipas':
            infoCard(20, 53);
            break;

        case 'Tlaxcala':
            infoCard(32, 75);
            break;

        case 'Veracruz':
            infoCard(5, 15);
            break;

        case 'Yucatán':
            infoCard(4, 45);
            break;

        case 'Zacatecas':
            infoCard(25, 24);
            break;
    }

}

/* State IDS */
var states = ['AGU', 'BCN', 'BCS', 'CAM', 'CHH',
    'CHP', 'COA', 'COL', 'DIF', 'DUR',
    'GRO', 'GUA', 'HID', 'JAL', 'MEX',
    'MIC', 'MOR', 'NAY', 'NLE', 'OAX',
    'PUE', 'QUE', 'ROO', 'SIN', 'SLP',
    'SON', 'TAB', 'TAM', 'TLA', 'VER',
    'YUC', 'ZAC'
];
/* State Names */
var state_names = ['Aguascalientes', 'Baja California Norte', 'Baja California Sur', 'Campeche', 'Chihuahua', 'Chiapas', 'Coahuila', 'Colima', 'Distrito Federal', 'Durango', 'Guerrero', 'Guanajuato', 'Hidalgo', 'Jalisco', 'Edo. Mexico', 'Michoacán', 'Morelos', 'Nayarit', 'Nuevo León', 'Oaxaca', 'Puebla', 'Queretaro', 'Quintana Roo', 'Sinaloa', 'San Luis Potosí', 'Sonora', 'Tabasco', 'Tamaulipas', 'Tlaxcala', 'Veracruz', 'Yucatán', 'Zacatecas'];
$(function() {
    $('.map').maphilight({
        fade: false
    });
});
$(document).ready(function() {
    $('.area').hover(function() {
        var id = $(this).attr('id');
        var state = $.inArray(id, states);
        $('#edo').html(state_names[state]);
        seleccionarEstado(state_names[state]);
    });

    $('.area').click(function() {
        var id = $(this).attr('id');
        var state = $.inArray(id, states);
        $('#edo').html(state_names[state]);
        seleccionarEstado(state_names[state]);
    });
});