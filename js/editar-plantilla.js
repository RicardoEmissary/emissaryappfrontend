function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            document.querySelector("#show-logo").setAttribute("src",e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
        
    }
}

$("#logo").change(function(){
    document.querySelector("#ningun-archivo").style.display = "none";
    readURL(this);
});

function handleChangeInForm(){
    let colorTema = document.getElementById("color-tema").value;
    let colorFuenteTema = document.getElementById("color-fuente-tema").value;
    let colorBoton = document.getElementById("color-boton").value;
    let colorFuenteBoton = document.getElementById("color-fuente-boton").value;
    let nota = document.getElementById("nota-opcional").value;
    let titulo = document.getElementById("titulo").value;
    
    document.getElementById("editar-plantilla-card-body").style.backgroundColor = colorTema;
    document.getElementById("editar-plantilla-card-body").style.color = colorFuenteTema;
    document.getElementById("editar-plantilla-card-btn").style.backgroundColor = colorBoton;
    document.getElementById("editar-plantilla-card-btn").style.color = colorFuenteBoton;
    document.getElementById("card-optional-text").innerHTML = nota;
    document.getElementById("card-title-show").innerHTML = titulo;
} 
handleChangeInForm();