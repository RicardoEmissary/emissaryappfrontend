var Meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo'];
var VentasData = [100000, 250000, 150000, 300000, 100000];
var ctx = document.getElementById("graphic-line-ventas-por-mes").getContext("2d");
var myGraphicBar = new Chart(ctx, {
    type: "line",
    data: {
        labels: Meses,
        datasets: [{
            data: VentasData,
            label: "Total ventas por mes",
            borderColor: "#ffffff",
            backgroundColor: '#ffffff', // Add custom color background (Points and Fill)
            borderWidth: 2, // Specify bar border width
            fill: false
        }]
    },
    options: {
        legend: {
            labels: {
                // This more specific font property overrides the global property
                fontColor: 'white'
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    fontColor: 'white',
                    beginAtZero: true
                }
            }],
            xAxes: [{
                ticks: {
                    fontColor: 'white',
                    beginAtZero: true
                }
            }]
        }
    }
});