$(document).ready(function() {
// Get buttons
var btnNext_step1 = document.getElementById("step1SaveBtn");
var btnNext_step2 = document.getElementById("step2SaveBtn");
var btnSave_step3 = document.getElementById("step3SaveBtn");
var btnBack_step2 = document.getElementById("step2BackBtn");
var btnBack_step3 = document.getElementById("step3BackBtn");

// When modal is opened, start in step 1
$('.startStepsNewOrder').click(function(e) {
	// hide old forms
	$("#step2Header").addClass("d-none");
	$("#step2Content").addClass("d-none");
	$("#step3Header").addClass("d-none");
	$("#step3Content").addClass("d-none");
	// show new form
	$("#step1Header").removeClass("d-none");
	$("#step1Content").removeClass("d-none");
});

// Go to step 2
btnNext_step1.onclick = function() {
	// hide old forms
	$("#step1Header").addClass("d-none");
	$("#step1Content").addClass("d-none");
	// show new form
	$("#step2Header").removeClass("d-none");
	$("#step2Content").removeClass("d-none");
}

// Go to step 3
btnNext_step2.onclick = function() {
	// hide old forms
	$("#step2Header").addClass("d-none");
	$("#step2Content").addClass("d-none");
	// show new form
	$("#step3Header").removeClass("d-none");
	$("#step3Content").removeClass("d-none");
}

// Go back to step 1
btnBack_step2.onclick = function() {
	// hide old forms
	$("#step2Header").addClass("d-none");
	$("#step2Content").addClass("d-none");
	// show new form
	$("#step1Header").removeClass("d-none");
	$("#step1Content").removeClass("d-none");
	
}

// Go back to step 2
btnBack_step3.onclick = function() {
	// hide old forms
	$("#step3Header").addClass("d-none");
	$("#step3Content").addClass("d-none");
	// show new form
	$("#step2Header").removeClass("d-none");
	$("#step2Content").removeClass("d-none");
	
}
})

